import React, { useState } from 'react';
import Menu from './components/Menu';
import ProductItem from './components/ProductItem';
import FormulaEditor from './components/FormulaEditor';
import './App.css';
import Watch1 from './assets/img/watch1.jpg';
import Watch2 from './assets/img/watch2.jpeg';
import Phone from './assets/img/phone.png';

const App = () => {
  const [products, setProducts] = useState([
    { id: 1, name: 'Smarty Watch', price: 10, formula: 'price + 10', updatedPrice: 20, img:Watch1 },
    { id: 2, name: 'Pretty phone', price: 20, formula: 'price * 2', updatedPrice: 40, img:Phone },
    { id: 3, name: 'Dumb Watch', price: 50, formula: '', updatedPrice: 50, img:Watch2 },
    // Add more products as needed
  ]);

  const handleFormulaChange = (productId, newFormula) => {
    setProducts((prevProducts) =>
      prevProducts.map((product) =>
        product.id === productId
          ? { ...product, formula: newFormula }
          : product
      )
    );
  };

  const handlePriceChange = (productId, newPrice) => {
    setProducts((prevProducts) =>
      prevProducts.map((product) =>
        product.id === productId
          ? { ...product, updatedPrice: newPrice }
          : product
      )
    );
  };

  const handleCancel = (productId, initialValue) => {
    setProducts((prevProducts) =>
      prevProducts.map((product) =>
        product.id === productId
          ? { ...product, formula: initialValue }
          : product
      )
    );
  };

  return (
    <div className="app">
      <Menu />
      <div className='main'>
        <h1>Apply price formula to my products</h1>
        <p>Showing 50 of 4589 products</p>
        {products.map((product) => (
          <div key={product.id} className='product-list'>
            <ProductItem
              name={product.name}
              price={product.price}
              picture={product.img}
            />
            <FormulaEditor
              initialValue={product.formula}
              onSave={(newFormula) =>
                handleFormulaChange(product.id, newFormula)
              }
              onPriceChange={(newPrice) =>
                handlePriceChange(product.id, newPrice)
              }
              currentPrice={product.price}
              onCancel={(initialValue) =>
                handleCancel(product.id, initialValue)
              }
            />
            <ProductItem
              name={product.name}
              price={product.updatedPrice}
              picture={product.img}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default App;