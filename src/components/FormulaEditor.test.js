// FormulaEditor.test.js
import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import FormulaEditor from './FormulaEditor';


test('allows editing the formula', () => {
  const onSaveMock = jest.fn();
  const { getByText, getByRole } = render(
    <FormulaEditor initialValue="price * 2" onSave={onSaveMock} />
  );
  const editButton = getByText(/edit/i);
  fireEvent.click(editButton);
  const input = getByRole('textbox');
  fireEvent.change(input, { target: { value: 'price + 10' } });
  fireEvent.click(getByText(/save/i));
  expect(onSaveMock).toHaveBeenCalledWith('price + 10');
});
