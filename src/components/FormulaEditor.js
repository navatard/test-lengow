import React, { useState, useEffect } from 'react';

const FormulaEditor = ({ initialValue, onSave, onCancel, onPriceChange, currentPrice }) => {
  const [formula, setFormula] = useState(initialValue);
  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    evaluatePrice();
  }, [formula, currentPrice]);

  const evaluatePrice = () => {
    try {
      // eslint-disable-next-line no-eval
      const newPrice = eval(formula.replace(/price/g, currentPrice));
      onPriceChange(newPrice);
    } catch (error) {
      console.error('Invalid formula');
    }
  };

  const handleSave = () => {
    // Perform validation here before saving
    onSave(formula);
    setEditMode(false);
  };

  const handleCancel = () => {
    setFormula(initialValue);
    setEditMode(false);
    onCancel(initialValue);
  };

  return (
    <div className="formula-editor">
      {editMode ? (
        <div>
          <input
            type="text"
            value={formula}
            onChange={(e) => setFormula(e.target.value)}
          />
          <button onClick={handleSave}>Save</button>
          <button onClick={handleCancel}>Cancel</button>
        </div>
      ) : (
        <div>
          <span>{formula}</span>
          <button onClick={() => setEditMode(true)}>Edit</button>
        </div>
      )}
    </div>
  );
};

export default FormulaEditor;