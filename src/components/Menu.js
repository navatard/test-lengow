import React from 'react';
import Logo from '../assets/img/logo.png';

const Menu = () => {

  return (
    <div className="menu">
      <div className="logo">
      <img src={Logo} alt='' />
      </div>
      <ul>
        <li>My products</li>
        <li>Prices formulas</li>
        <li>Market position</li>
        <li>Settings</li>
      </ul>
    </div>
  );
};

export default Menu;