import React from 'react';

const ProductItem = ({ name, price, picture }) => {

  return (
    <div className="product-item">
      <p>{name}</p>
      <img src={picture} alt='' />
      <p>Original price  : <br/>{price}</p>
    </div>
  );
};

export default ProductItem;